# Our Tutorials

## Crash Report Guide

If your game crashes and you require assistance, we advise that you grab the crash report that is automatically generated when this occurs. The steps below will guide you to retrieve this depending on the launcher you use.

#### Twitch Launcher
**1:** Open the Launcher  
**2:** Select the modpack and navigate to the `...` icon in the top right. Press this and click `Open Folder`  
**3:** You are now in the root folder of the modpack, open the `crash-reports` folder.  
**4:** Find the latest crash report and copy the whole content and paste it to [Pastebin](https://pastebin.com).  
**5:** On Pastebin, you click on the `Create new paste` button and copy the web link.  
**6:** Paste the link where it is required (e.g. Discord or on your ticket).

#### ModRealms Launcher
**1:** Open the Launcher  
**2:** Right click the modpack and select the `View crash-reports` button.  
**3:** You are now in the `crash-reports` folder. Find the latest report and open it.  
**4:** Copy the whole content and paste it to [Pastebin](https://pastebin.com).  
**5:** On Pastebin, you click on the `Create new paste` button and copy the web link.  
**6:** Paste the link where it is required (e.g. Discord or on your ticket).

## Claiming Land

> On ModRealms, we use the most up to date version of Grief Prevention that is running on Sponge to claim land.

#### How to Claim Land

In order to claim land, you must have a **Golden Shovel** in possession. You can get one of these when you first join the server. If you lose the shovel early on, you can run the command `/claimtool` to receive one for free. You must also have a considerable amount of Claim Blocks **(1 claim block = 1 block)**. To get claim blocks, you can buy them the tokenshop, or from the store. You also gain 120 claim blocks per hour just by playing!

In Grief Prevention, there are two methods of claiming land, either `2D` or `3D`.

> To switch between them, you can use /cuboid. The chat will tell you which one you have toggled.

- `2D` means that it doesn't matter what level you claim the land on, It will claim from the bottom of the world to the max build level (y256)
- `3D` means that it will ONLY claim what is in the cuboid area that you have selected.

Once you have a method selected, if you are using the 2D mode, right click the two opposite corners of the claim that you are wishing to build in. If you are in 3D mode, you still need to right click the two opposite corners; one at the lowest point of the claim and the other at the highest point. Please note that 10x10 is the smallest size that you can claim (100 claimblocks)

To check who has claimed an area of land, grab a stick and right click the air/area of the land. This will tell you the details of the claim that you are looking at. If it says 'Wilderness', the area isn't claimed by another user.


#### How to Subdivide Claims:
- When you subdivide your claim, it means that you are creating smaller claims in your current claim to allow different people to have different build/access permissions at different points of your base. 
- To subdivide, hold your golden shovel in your hand and run the command /subdivideclaims. This allows your golden shovel to create smaller claims in the larger existing one. 
- Claiming in subdivision always works in 2D mode so it claims to the maximum height of the main claim. When you select two corners, you will notice iron blocks instead of golden blocks. 

> Please note that subdivision does not charge you for any claimblocks as the land had originally been claimed using them. 

- To get out of subdivide mode, change to a different item in your hotbar or run the subdivisionclaims command again. This feature can be used to sell plots of lands and/or make towns for currency (£ not tokens).

#### Important Commands:

- `/claimsinfo` to check information of the claim such as trusted players or the owner.
- `/claimslist` to see a list of your claims.
- `/permissiontrust <name>` while standing in the claim to allow someone permissions to trust others and run claim commands in your claim
- `/trust <name>` while standing in the claim to allow someone to build in your claim, and access chests and other inventories. This command is the general trust.
- `/containertrust <name>` while standing in the claim to allow someone to access chests, farm crops, tame/kill animals, use of a bed, and use of level/button/pressure plate.
- `/accesstrust <name>` while standing in the claim to allow someone to access your bed and inventories like an anvil.
- `/untrust <name>` removes the permissions of a person on the claim you are standing in.
- `/untrustall <name>` removes the permissions of player from all your clams.
- `/trustlist` shows you a list of those you have trusted in the claim you are standing in.
- `/abandonclaim` this abandons the claim you are standing in
- `/abandonallclaims` this abandons all of your existing claims
> Please note that all of these commands also work for subdivided claims


## Contacting Staff

> ModRealms' Staff team is rapidly growing and is always adopting new members to help out on the network. There are various different ways to contact them if ever you are in need of help.

#### Getting Help on Discord
Receiving help on our Discord is very easy, there is usually a member of staff online at all times throughout the day. In order to talk to a staff member, you can do two things:

- In our support channels that relate to your issue (e.g. #support-general for network, #launcher-support for launcher), you can simply type your question/issue in that channel. Members of our community or the staff team can then view that issue and resolve it as fast as possible. However, if you would like it to be seen quicker, you can tag the @Network Staff role which will ping all staff currently online about your issue.
- We have a dedicated bot on the Discord that can be used for direct communication to our staff team. To use this, you just need to directly message the 'ModRealms Bot' (in PM) with the issue that you would like to report. This will then send a message to our staff Discord with your name and the message content. Our staff can then respond to you through the bot's DM system so you can keep into contact with the member of staff until the issue is fixed.

 **If a staff member is not online, never fear to ask your question to the other players. I'm sure that there are many lovely people who would be willing to answer your questions or provide support.**

## Joining and Using our Discord

During the March relaunch, the ModRealms Discord had a major redesign to ensure that all players who joined would be given all of the necessary information straight from the beginning. The information might include helpful links, Discord rules, staff roles throughout the network. Our Discord is our main branch of communication over the forum and all announcements (large or small) are usually posted here first. It features many custom features that we feel make your experience more worth while and unique.

#### How to connect to our Discord
If you do not already have a Discord account, you will need to create one here: https://discordapp.com/register. This will allow you to be identified when you enter our Discord server. Now, you can either do two things. Discord allows you to either run the program in your browser or download it locally. We recommend using the installed program as it is a lot more convenient to use and allows for more flexibility when in use (And it's just cool!).

When you have logged into Discord in either the program or browser, you will then need to use our invite link in order to join our Discord server. You can find the link by either running /discord ingame, having an existing member create a link for you or by using the 'Discord' button at the top of our forum. Here is a permanent invite link that you can use: https://discord.gg/tKKeTdc. Entering this link into your browser's address bar will automatically connect you to the server in your program and will give you the option to also open the web-based Discord.

Upon entering the server, you will be in our #welcome-channel. This contains all of the useful information that you can use to get started. However you may realise that you're only given a few channels that you can type in (#general-chat, #hangout, #network-bridge and the various support channels). We have a feature in our Discord that allows you to opt-in for a server-specific role that will allow us to target certain servers in announcements and also allow users to have server-specific chats throughout our Discord. To apply your role, you will need to follow the instructions in the 'Getting Started' section of the #welcome-channel. You will need to 'react' to the message depending on the server that you wish to assign your self to. Upon reacting you'll gain access to the chats related to that server.

#### So you're in the Discord, What do you do now?
We have various channels that all suit different purposes (This will not show all of the channels as they will change over time):

`#important-news` - This channel contains all announcements that will be posted about the whole network  
`#server-information` - This channel contains all of the server information that is required for you to connect (e.g. modpack, IP, banned items list etc)  
`#join-leave` - This logs the joining/leaving of members through the Discord.  

`#general-chat` - This section is for general discussion related to ModRealms. All discussion not related to the network will need to be posted in the hangout chat.  
`#hangout-chat` - This chat is for discussion related to topics that aren't linked to ModRealms. They can be, but anything that isn't related to ModRealms and follows the rules is allowed in here.  
`#network-bridge` - This bridge connects the chat between the network and discord. Feel free to chat with the players at any time!  
`Chatterboxes Voice` - This channel is the general voice chat which you can connect to if you wish to discuss anything ModRealms related.  

#### This is a bit boring... Where are the commands?
On our Discord, We only have one bot. This is the ModRealms Bot that was designed by us, for us. Running the command mr-help will send the user a list of commands that is registered in the bot; Some of these are not run-able as a user. Below will list some of the important commands that can be used on our Discord. These commands can be run anywhere in our Discord that isn't #network-bridge

`mr-wiki` - This command displays the knowledgebase link which this thread is found in.  
`mr-vote` - Shows all of our voting links for the network  
`mr-mojang` - Shows a prompt with the status of the Mojang servers  
`mr-launcher` - Shows a message with links that can be used to download the ModRealms Launcher  
`mr-status` - This sends the user messages that show live details about all of our servers that are running. (Very neat!)  

**We hope you enjoy your stay in our Discord!**

## Utilising Chunkloaders

> On ModRealms we use a custom version of BetterChunkLoader to work with our servers and is optimized to fit the network. The Staff team often gets questions about creating chunkloaders using this plugin.

#### How to Create Chunkloaders:
There are two types of chunkloaders that are made with two different blocks, **Diamond Block** and **Iron Block** which make `Always On` and `Online` chunkloaders respectively.
- `Always On` chunkloaders will remain active when you log off the server, this allows automated processes to keep running even when you are offline.
- `Online` chunkloaders are only active when you are on the server playing, this allows you to explore the world and otherwise leave your base and allow your processes to continue running; however, if you log off the server the chunkloader will become inactive.

To create these chunkloaders you simply need an **Iron Block** for an Online chunkloader, and a **Diamond Block** for the **Always On** chunkloader, then you will need a **Blaze Rod**. To activate the chunkloader you right-click on the block with the Blaze Rod which will bring up an gui that holds five Glass Bottles. Depending on the amount of chunkloading credits you have you can choose from five options. To check your current balance you can type `/bcl bal`, once you know you have the appropriate credits you can choose whatever size you would like to load (keep in mind that the more chunks that are loaded, the more stress is put on the server, however our servers are well-equipped to handle many loaded chunks).

Once you've selected and clicked on the size that you would like, the chunkloader is created and is now loading chunks in the area that you specified.

> Note: Loaded chunks are loaded from Bedrock to the Sky Limit, there is no way to change the loaded area to a cube.

#### How to Remove Chunkloaders:
To remove a chunkloader simply break the Diamond Block or Iron Block and the chunkloader will be removed from the world and your balance will be changed to reflect the credits from the chunkloader.

> Note: If a chunk-loaded area is causing too much stress on the server, Staff have the right to remove the chunkloaders - which will be credited to your balance.

#### Important Commands:
`/bcl bal` to check your current chunkloading credit balance.  
`/bcl list` to check your currently active chunkloaders.