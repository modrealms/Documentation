# About Us

ModRealms is a modded network that launched in 2012 and has been evolving over the years to bring you the most up to date modpacks available in the modded community. We have a variety of gameplay enhancing features to ensure that your experience is premium at all times. We have a mixture of custom features such as a fully Discord-integrated tickets system and a kit-store unique to each server! Since our relaunch, we have been amazed by our friendly, and helpful community here at ModRealms, and are excited for the future endeavours of the network.

# Our Promises

- Reliable stability, performance and server uptime
- Dedicated Support through Discord and In-game
- Fully Working (Grief) Protection
- Long-Term Availability and Around for Years
- Natural, yet Exciting Modpack Experience
- Latest and Greatest Modpacks

# Network Rules

These rules apply to all of the services that we provide. When you use our service, you are agreeing to these rules. Staff are given guidelines of punishments and any punishment they may give is final and at their discretion. 

- No Spamming, Advertising or Excessive Cap Use.
- No Griefing of any Protected Areas.
- No Foul Language, Hate Speech or Sexual Comments.
- Cheating, Hacking, Glitching or Duping is not allowed.
- Only English in the Main Chat.
- Impersonation of another Player or Staff Member is not allowed.
- Any form of Bullying, Nazism or Racism is not Tolerated at any costs.
- No refunds of items lost, unless caused by a proven glitch with our services.
- Chargebacks are not allowed and will result with a removal from the network.
- Abusing a store product or a purchased perk is not allowed.
- All transactions are final, if you lose the perks due to an outage or a suspension of features, we do not need to compensate.

> These rules are not excessive, If any staff member feels that a player's behavior is affecting the players or network negatively, they have the right to warrant a punishment and/or remove you from the network. For the full terms and conditions, please click [here](https://modrealms.net/tos.html).

# Voting Links

When you vote for us, you will recieve a certain amount of orbs that can then either be used on Discord or In-game. Depending on the server, you will also receive a random modded item that can be used on the server. Voting is one of the best ways to support us as it helps us grow our community and continue to provide our premium experience. 

- FTBServers: [Link 1](https://ftbservers.com/server/qV75biQg/vote)
- MinecraftMP: [Link 2](http://minecraft-mp.com/server-s191432)
- Minecraft-Server-List: [Link 3](https://minecraft-server-list.com/server/426244/vote/)
- TOPG: [Link 4](https://topg.org/Minecraft/in-491515)
- MinecraftServer: [Link 5](https://minecraft-server.net/vote/modrealms)
