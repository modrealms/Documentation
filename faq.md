# Frequently Asked Questions

## What's the difference between Online and AlwaysOn Chunkloading?

> **Online** means that the Chunk Loader only works when you are logged into the server. Whereas **Alwayson** means that the chunk loader will run if you are online or offline.

## Where can I see a list of Banned Items?

> To find a list of banned items, you can run `/whatsbanned` or `/banneditems` ingame.

## How do I create a ticket?

> To create a ticket, you must have your player verified with Discord. You can do this by using `/verify` and following the instructions. After that, you can then run `/tickets create <message>` (Replacing `<message>` with your issue). This will create your ticket which you can then access through Discord in a private channel that was created for you. Any events that occur through Discord (such as replying) will also take affect ingame. To view all of your open or closed tickets, you can use `/tickets`.

## What are Orbs and How can I get them?

> Orbs are used as our premium currency that can be earned through either voting, donating or through reviewing us on our server listing. Orbs can be used in our ingame store to buy many different modded kits and equipment. You can use `/orbs store` to get started.

## Can I contribute to the documentation?

> Of course! If you wish to submit a change/addition, feel free to create a Merge Request [here.](https://gitlab.com/modrealms/Documentation/blob/master/) (GitLab)