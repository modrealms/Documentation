# Our Launcher

On ModRealms, we have been working to develop the perfect launcher for our network. We feel that we have finally managed to create one that you'll love! The ModRealms launcher is using the SKLauncher framework with a custom skin to give a more personalised feel and includes all of the modpacks that are included on our network. Having a custom launcher means that we are able to push updates that we feel will be beneficial to your gameplay and experience whilst on our servers. Some tweaks include:

- Authentication Mod added to ensure you do not receive session errors.
- Startup Arguments have been tweaked to improve RAM usage.
- UpLink Mod which allows other users across Discord to see what server you are playing!
- Optional Resource-Packs with the unique patches for each modpack!
- LagGoggles comes pre-installed on each modpack!`

This launcher includes automatic updates that will take place when you restart the client and updates to a modpack may be frequent depending on the fixes/tweaks that we push. 

> Please note, We will only be able to provide limited support for the players not using our launcher. Below, will contain all of the links to our launcher and a portable version will be arriving soon:

### Download Links

Universal Version: [Click here.](https://files.modrealms.net/launcher/ModRealmsLauncher-2.0-Universal.jar)  
Windows Version (exe): [Click here.](https://files.modrealms.net/launcher/ModRealmsLauncher-2.0.exe)  
Portable Version (Custom installation location): [Click here.](https://files.modrealms.net/launcher/ModRealmsLauncher-2.0-Portable.zip)
> If you're using the portable version, be sure to read the instructions after you have unzipped the file! (Don't delete the `portable.txt` file!)

### 64-Bit Java

Our launcher requires everybody to be running Java 64-Bit. Otherwise, the launcher will not be able to allocate the correct amount of RAM to the modpack. To check if you are running the correct version of Java, Open your `Command Prompt` and run `java -version`. The result should contain `64-Bit Server`.

    java version "1.8.0_172"
    Java(TM) SE Runtime Environment (build 1.8.0_172-b11)
    Java HotSpot(TM) 64-Bit Server VM (build 25.172-b11, mixed mode)

To download Java SE 8 Update 181, [Please click this link.](https://goo.gl/mEDmS8) 
### Frequently Asked Questions
>  
#### Help! My Antivirus is alerting me when using the Windows version!
 No need to worry! Due to the amount of downloads and the signing process of the launcher (being an .exe), many Anti-Viruses may try to block the launcher from your computer. If this happens to occur, please whitelist the launcher file in your firewall. However, if you do not wish to do this, you can use the Universal version instead.
#### Where can I ask for help if it's not working?
 If you contact us on [Our Discord](https://discord.gg/tKKeTdc), We can work to get your issue fixed as swiftly as possible. If you also wish, you can create a ticket if you are already ingame.
#### Can I have an account for me and my other friend on the launcher?
 Of course! There is a button for switching accounts (when logged in), whilst there, you can remove/add any accounts that you wish and switch between them!
#### What can I do if a modpack won't download?
 If you message one of the ModRealms staff, they can work to get you up and running as soon as possible! Be sure to check your internet settings beforehand to ensure that it isn't causing any problems.
