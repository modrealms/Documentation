# Our Servers

Across the network, there are certain items that are banned to improve server performance and gameplay experience. To view the list, either run `/whatsbanned` ingame or go [here](https://goo.gl/zfpsG3)

#### Hub

> The server is currently running on version `1.8 - 1.12.2+`

- Server Hostname: `hub.modrealms.net`  
- Modpack: `Minecraft Forge 1.12 (N/A)`


## 1.12 Servers

#### RevelationRealms

> The server is currently running on version `2.5.0`

- Server Hostname: `revelation.modrealms.net`  
- Modpack: `FTB Revelation`

#### EnigmaticaSkyRealms

> The server is currently running on version `1.6`

- Server Hostname: `enigmasky.modrealms.net`  
- Modpack: `Enigmatica 2: Expert Skyblock`

#### DireRealms112

> The server is currently running on version `2.3.0`

- Server Hostname: `dire112.modrealms.net`  
- Modpack: `FTB Presents Direwolf20 1.12`

#### ContinuumRealms

> The server is currently running on version `1.4.1`

- Server Hostname: `continuum.modrealms.net`  
- Modpack: `FTB Continuum`

#### HorizonsRealms

> The server is currently running on version `1.8.1`

- Server Hostname: `horizons.modrealms.net`  
- Modpack: `FTB Horizons III`