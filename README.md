# ModRealms Documentation

Welcome to the Documentation. Around this site, you will find various pieces of information that will help you get started on ModRealms. If you would like to contribute, you can create a Merge Request [here](https://gitlab.com/modrealms/Documentation/merge_requests/new).

### Useful Links:
- [Facebook](https://www.facebook.com/modrealms/)
- [Discord](https://discord.gg/tKKeTdc)
- [Twitter](https://twitter.com/ModRealms)
- [Youtube](https://www.youtube.com/channel/UCx_iGrTo1z_ZuOf2w_Lfl1Q)
- [Website](https://modrealms.net)
- [Server IPs](servers.md)